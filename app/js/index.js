const { ipcRenderer } = require('electron');

const about = document.querySelector('#about');

about.addEventListener('click', ()=> {
    ipcRenderer.send('about');
})