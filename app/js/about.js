const { ipcRenderer } = require('electron');

const about = document.querySelector('#exit-about');

about.addEventListener('click', ()=> {
    ipcRenderer.send('exit-about');
})