const { app, BrowserWindow, ipcMain } = require('electron');

let win;
let about;

createWindow = () => {
    win = new BrowserWindow({
        width: 800,
        height: 600,
        resizable: false,
        webPreferences: {
            nodeIntegration: true,
        }
    })
    win.loadURL(`file://${__dirname}/app/index.html`);
    win.removeMenu();
    win.webContents.openDevTools();
};

app.whenReady().then(createWindow);

app.on('window-all-closed', ()=> {
    if(process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', ()=> {
    if(BrowserWindow.getAllWindows().length === 0) {
        createWindow();
    }
});

ipcMain.on('about', ()=> {
    if(about == null) {
        about = new BrowserWindow({
            width: 300,
            height: 300,
            parent: win,
            frame: false,
            resizable: false,
            webPreferences: {
                nodeIntegration: true
            }
           //alwaysOnTop: true
        });
        about.on('closed', ()=> {
            about = null;
        })
    }
    about.removeMenu();
    about.loadURL(`file://${__dirname}/app/about/about.html`);
})

ipcMain.on('exit-about', ()=> {
    about.close();
})